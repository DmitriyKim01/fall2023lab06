package backend;
// Dmitriy Kim 2232931
import java.util.Random;

public class RpsGame {
    //TODO
    private int wins;
    private int ties;
    private int losses;
    private Random random;

    public RpsGame () {
        wins = 0;
        ties = 0;
        losses = 0;
        random = new Random();
    }

    public int getWins() {
        return wins;
    }

    public int getTies() {
        return ties;
    }

    public int getLosses() {
        return losses;
    }

    public String playRound (String playerChoice) {
        String computerChoice = "rock";
        String message = "";
        int computerNum = this.random.nextInt(3);
        switch (computerNum) {
            case 1:
                computerChoice = "scissors";
                break;
            case 2:
                computerChoice = "paper";
                break;
        }
        System.out.println(playerChoice + " vs " + computerChoice + "\n");
        if (playerChoice.equals(computerChoice)) {
            message = "It's a tie!";
            this.ties++;
        } else if ((playerChoice.equals("rock") && computerChoice.equals("scissors")) ||
                   (playerChoice.equals("scissors") && computerChoice.equals("paper")) ||
                   (playerChoice.equals("paper") && computerChoice.equals("rock"))) {
            message = "Player wins!";
            this.wins++;
        } else {
            message = "Computer wins!";
            this.losses++;
        }
        System.out.println(message);
        return message;
    }
}
