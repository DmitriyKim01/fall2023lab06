package rpsgame;
// Dmitriy Kim 2232931
import java.util.Scanner;

import backend.RpsGame;

/**
 * Console application for rock paper scissors game
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //TODO
        String input = "";
        Scanner scan = new Scanner(System.in);
        RpsGame gameController = new RpsGame();
        do {
            clear();
            System.out.println("Enter your choice: rock/scissors/paper");
            System.out.print("Your choice: ");
            input = scan.next().toLowerCase();
            clear();
            if (!input.equalsIgnoreCase("quit")) {
                gameController.playRound(input);
                System.out.println("Wins: " + gameController.getWins());
                System.out.println("Losses: " + gameController.getLosses());
                System.out.println("Ties: " + gameController.getTies());
            enter(scan);
            }
        } while (!input.equalsIgnoreCase("quit"));
        clear();
        System.out.println("Have a great day!");
    }

    public static void clear () {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void enter (Scanner scan) {
        System.out.print("\nPress enter to continue...");
        scan.nextLine();
        scan.nextLine();
    }
}
